#include "main.h"
#include "OpenGLContext.h"
#include "Utilities.h"

OpenGLContext				openGLContext;
bool						b_running = true;

HINSTANCE					hInstance;

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_SIZE:
		openGLContext.reshapeWindow(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hwnd, message, wParam, lParam);
}

bool createWindow(LPCWSTR p_title, int p_width, int p_height)
{
	WNDCLASS windowClass;
	HWND hWnd;
	DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;

	hInstance = GetModuleHandle(NULL);

	windowClass.style			=		CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc		=		(WNDPROC)WndProc;
	windowClass.cbClsExtra		=		0;
	windowClass.cbWndExtra		=		0;
	windowClass.hInstance		=		hInstance;
	windowClass.hIcon			=		LoadIcon(NULL, IDI_WINLOGO);
	windowClass.hCursor			=		LoadCursor(NULL, IDC_ARROW);
	windowClass.hbrBackground	=		NULL;
	windowClass.lpszMenuName	=		NULL;
	windowClass.lpszClassName	=		p_title;

	if (!RegisterClass(&windowClass))
	{
		return false;
	}

	hWnd = CreateWindowEx(	dwExStyle, p_title, p_title,
							WS_OVERLAPPEDWINDOW,
							CW_USEDEFAULT,
							0, p_width, p_height,
							NULL,
							NULL,
							hInstance,
							NULL);

	openGLContext.createOpenGLContextv30(hWnd); 

	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);

	return true;
}


int WINAPI WinMain(	HINSTANCE				hInstance,
					HINSTANCE				hPrevInstance,
					LPSTR					lpCmdLine,
					int						cmdShow)
{
	MSG msg;

	char * cStrTitle = "GL 3 Base Renderer";
	size_t origSize = strlen(cStrTitle) + 1;

	const size_t newSize = 128;
	size_t convertedChars = 0;

	wchar_t wcstring[newSize];
	mbstowcs_s(&convertedChars, wcstring, origSize, cStrTitle, _TRUNCATE);

	createWindow(wcstring, 960, 576);

	openGLContext.setupScene();

	while (b_running)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				b_running = false;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			openGLContext.renderScene();
		}
	}



	return (int) msg.wParam;
}