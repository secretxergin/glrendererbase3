#include "ModelInfoOBJ.h"
#include "Utilities.h"

ModelInfoOBJ::ModelInfoOBJ()
{

}

ModelInfoOBJ::ModelInfoOBJ(const char* p_filename)
{
	init(p_filename);
}

ModelInfoOBJ::~ModelInfoOBJ()
{

}

void ModelInfoOBJ::init(const char* p_filename)
{
	FILE * fileToRead;
	fopen_s(&fileToRead, p_filename, "rt");
	if (fileToRead != NULL)
	{
		unsigned int numTris = 0, numQuads = 0;

		std::vector <VertexPositionInfo> rawVerts;
		std::vector <VertexUVInfo> rawUVs;
		std::vector <VertexNormalInfo> rawNormals;

		std::vector <IndexInfo> triangles;

		// + Start Parse Loop
		char buffer[128];
		while (fgets(buffer, 128, fileToRead))
		{
			// Parse Vertex Position Info
			VertexPositionInfo vert;
			if (sscanf_s(buffer, "v %f %f %f", &vert.x, &vert.y, &vert.z) == 3)
			{
				rawVerts.push_back(vert);
			}
			// Parse  Vertex UV Info
			VertexUVInfo texCoords;
			if (sscanf_s(buffer, "vt %f %f", &texCoords.u, &texCoords.v) == 2)
			{
				rawUVs.push_back(texCoords);
			}
			// Parse Vertex Normal Info
			VertexNormalInfo normal;
			if (sscanf_s(buffer, "vn %f %f %f", &normal.nx, &normal.ny, &normal.nz) == 3)
			{
				rawNormals.push_back(normal);
			}
			//Parse Face Info
			unsigned int  numArgs;
			IndexInfo index[4];
			numArgs = sscanf_s(buffer, "f %i/%i%/%i %i/%i%/%i %i/%i%/%i %i/%i%/%i", &index[0].pos, &index[0].uv, &index[0].normal,
																					&index[1].pos, &index[1].uv, &index[1].normal,
																					&index[2].pos, &index[2].uv, &index[2].normal,
																					&index[3].pos, &index[3].uv, &index[3].normal);
			if (numArgs == 9) // TRIANGLE
			{
				triangles.push_back(index[2]);
				triangles.push_back(index[1]);
				triangles.push_back(index[0]);
				numTris++;
			}

			if (numArgs == 12) // QUAD
			{
				// Split into two triangles
				triangles.push_back(index[2]);
				triangles.push_back(index[1]);
				triangles.push_back(index[0]);

				triangles.push_back(index[0]);
				triangles.push_back(index[3]);
				triangles.push_back(index[2]);
				numQuads++;
			}
		}
		// - End Parse Loop

		Utils::Log("Vertex Position Count: %i\n", rawVerts.size());
		Utils::Log("Vertex Normal Count: %i\n", rawNormals.size());
		Utils::Log("Vertex UV Count: %i\n", rawUVs.size());
		Utils::Log("%i Triangles, %i Quads\n", numTris, numQuads);
		Utils::Log("Total Triangles: %i\n", triangles.size()/3);

		// Rearrange so that indices for each vertex attribute are aligned
		unsigned int currentIndex = 0;
		for (auto	iterator_triangles = triangles.begin();
					iterator_triangles != triangles.end();
					++iterator_triangles)
		{
			IndexInfo indexToProcess = (IndexInfo)*iterator_triangles;

			// + Check if a Duplicate
			unsigned int currentDuplicateIndex = 0;
			bool bIsDuplicate = false;
			for (int iterator_duplicates = 0; iterator_duplicates < m_positions.size(); ++iterator_duplicates)
			{
				if (	(rawVerts[indexToProcess.pos -1].x == m_positions[iterator_duplicates].x) &&
						(rawVerts[indexToProcess.pos -1].y == m_positions[iterator_duplicates].y) &&
						(rawVerts[indexToProcess.pos -1].z == m_positions[iterator_duplicates].z) &&
						(rawUVs[indexToProcess.uv - 1].u == m_uvs[iterator_duplicates].u) &&
						(rawUVs[indexToProcess.uv - 1].v == m_uvs[iterator_duplicates].v) &&
						(rawNormals[indexToProcess.normal - 1].nx == m_normals[iterator_duplicates].nx) &&
						(rawNormals[indexToProcess.normal - 1].ny == m_normals[iterator_duplicates].ny) &&
						(rawNormals[indexToProcess.normal - 1].nz == m_normals[iterator_duplicates].nz))
				{
					bIsDuplicate = true;
					currentDuplicateIndex = iterator_duplicates;
					break;
				}
			}
			// - End Duplicate Checking

			// Process!
			if (bIsDuplicate)
			{
				m_indices.push_back(currentDuplicateIndex);
				/*
				Utils::Log("DUPLICATE DATA %i:\n", currentIndex);
				Utils::Log("\torig: %f, %f, %f\n", rawVerts[indexToProcess.pos - 1].x, rawVerts[indexToProcess.pos - 1].y, rawVerts[indexToProcess.pos - 1].z);
				Utils::Log("\tcopy: %f, %f, %f\n", m_positions[currentDuplicateIndex].x, m_positions[currentDuplicateIndex].y, m_positions[currentDuplicateIndex].z);
				Utils::Log("\torig: %f, %f\n", rawUVs[indexToProcess.uv - 1].u, rawUVs[indexToProcess.uv - 1].v);
				Utils::Log("\tcopy: %f, %f\n", m_uvs[currentDuplicateIndex].u, m_uvs[currentDuplicateIndex].v);
				Utils::Log("\torig: %f, %f, %f\n", rawNormals[indexToProcess.normal - 1].nx, rawNormals[indexToProcess.normal - 1].ny, rawNormals[indexToProcess.normal - 1].nz);
				Utils::Log("\tcopy: %f, %f, %f\n", m_normals[currentDuplicateIndex].nx, m_normals[currentDuplicateIndex].ny, m_normals[currentDuplicateIndex].nz);
				*/
			}
			else
			{
				m_indices.push_back(currentIndex);
				/*
				Utils::Log("VERTEX %i:\n", currentIndex);
				Utils::Log("\t%f, %f, %f\n", rawVerts[indexToProcess.pos - 1].x, rawVerts[indexToProcess.pos - 1].y, rawVerts[indexToProcess.pos - 1].z);
				Utils::Log("\t%f, %f\n", rawUVs[indexToProcess.uv - 1].u, rawUVs[indexToProcess.uv - 1].v);
				Utils::Log("\t%f, %f, %f\n", rawNormals[indexToProcess.normal - 1].nx, rawNormals[indexToProcess.normal - 1].ny, rawNormals[indexToProcess.normal - 1].nz);
				*/
				m_positions.push_back(rawVerts[indexToProcess.pos-1]);
				m_uvs.push_back(rawUVs[indexToProcess.uv-1]);
				m_normals.push_back(rawNormals[indexToProcess.normal-1]);
				++currentIndex;
			}
		}

		Utils::Log("Number of Vertex entries: %i\n", m_positions.size());
		Utils::Log("Number of Index entries: %i\n", m_indices.size());
	}
	else
	{
		Utils::Log("Failed to open %s\n", p_filename);
	}
}

unsigned int ModelInfoOBJ::getPositionFloatCount()
{
	return (m_positions.size() * 3);
}

unsigned int ModelInfoOBJ::getUVFloatCount()
{
	return (m_uvs.size() * 2);
}

unsigned int ModelInfoOBJ::getNormalFloatCount()
{
	return (m_normals.size() * 3);
}

unsigned int ModelInfoOBJ::getIndexCount()
{
	return (m_indices.size());
}


void ModelInfoOBJ::copyPositionDataToBuffer(float * p_buffer)
{
	for (unsigned int iterator = 0; iterator < m_positions.size(); ++iterator)
	{
		p_buffer[(iterator * 3) + 0] = m_positions[iterator].x;
		p_buffer[(iterator * 3) + 1] = m_positions[iterator].y;
		p_buffer[(iterator * 3) + 2] = m_positions[iterator].z;
	}
}

void ModelInfoOBJ::copyUVDataToBuffer(float * p_buffer)
{
	for (unsigned int iterator = 0; iterator < m_uvs.size(); ++iterator)
	{
		p_buffer[(iterator * 2) + 0] = m_uvs[iterator].u;
		p_buffer[(iterator * 2) + 1] = m_uvs[iterator].v;
	}
}

void ModelInfoOBJ::copyNormalDataToBuffer(float * p_buffer)
{
	for (unsigned int iterator = 0; iterator < m_normals.size(); ++iterator)
	{
		p_buffer[(iterator * 3) + 0] = m_normals[iterator].nx;
		p_buffer[(iterator * 3) + 1] = m_normals[iterator].ny;
		p_buffer[(iterator * 3) + 2] = m_normals[iterator].nz;
	}
}

void ModelInfoOBJ::copyIndexDataToBuffer(int * p_buffer)
{
	for (unsigned int iterator = 0; iterator < m_indices.size(); ++iterator)
	{
		p_buffer[iterator] = m_indices[iterator];
	}
}