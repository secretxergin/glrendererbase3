#pragma once

#include "main.h"
#include "Shader.h"

static float yrot = 0.0f;

class OpenGLContext
{
protected:
	HGLRC				m_hrc;
	HDC					m_hdc;
	HWND				m_hwnd;

private:
	int					m_windowHeight;
	int					m_windowWidth;

	Shader*				m_shader;

	// + Matrices
	glm::mat4			m_projectionMatrix;
	glm::mat4			m_viewMatrix;
	glm::mat4			m_modelMatrix;
	// - Matrices

	// + Vertex Objects
	unsigned int		m_vertexArrayObject[1];
	unsigned int		m_vertexBufferObject[3];
	unsigned int		m_textures[4];
	// - Vertex Objects

	// + Temporary Geometry
	void				createQuad(void);
	// - Geometry
	void				loadOBJ(const char* p_filename);

public:
	OpenGLContext(void);
	OpenGLContext(HWND p_hwnd);
	~OpenGLContext(void);

	bool				createOpenGLContextv30(HWND p_hwnd);
	void				reshapeWindow(int p_width, int p_height);
	void				setupScene(void);
	void				renderScene(void);

};