#include "Shader.h"
#include "Utilities.h"

Shader::Shader(void)
{

}

Shader::Shader(const char* p_vsFile, const char* p_fsFile)
{
	init(p_vsFile, p_fsFile);
}

Shader::~Shader(void)
{
	glDetachShader(shader_id, shader_fp);
	glDetachShader(shader_id, shader_vp);

	glDeleteShader(shader_fp);
	glDeleteShader(shader_vp);

	glDeleteProgram(shader_id);
}

void Shader::init(const char* p_vsFile, const char* p_fsFile)
{
	shader_vp = glCreateShader(GL_VERTEX_SHADER);
	shader_fp = glCreateShader(GL_FRAGMENT_SHADER);

	const char* vsText = Utils::ReadTextFile(p_vsFile);
	const char* fsText = Utils::ReadTextFile(p_fsFile);

	if (vsText == NULL || fsText == NULL)
	{
		Utils::Log("Error in reading shader file!");
		return;
	}

	glShaderSource(shader_vp, 1, &vsText, 0);
	glShaderSource(shader_fp, 1, &fsText, 0);

	glCompileShader(shader_vp);
	validateShader(shader_vp, p_vsFile);
	glCompileShader(shader_fp);
	validateShader(shader_fp, p_fsFile);

	free((void*)vsText);
	free((void*)fsText);

	shader_id = glCreateProgram();
	glAttachShader(shader_id, shader_vp);
	glAttachShader(shader_id, shader_fp);
	glLinkProgram(shader_id);
	validateProgram(shader_id);


	// ----------------------------------------
	// |      Shader Attribute Binding        |
	// ----------------------------------------
	glBindAttribLocation(shader_id, 0, "in_Position");
	glBindAttribLocation(shader_id, 1, "in_Color");
	glBindAttribLocation(shader_id, 2, "in_UVs");

}

unsigned int Shader::id(void)
{
	return shader_id;
}

void Shader::bind(void)
{
	glUseProgram(shader_id);
}


void Shader::unbind(void)
{
	glUseProgram(0);
}


void Shader::validateShader(GLuint p_shader, const char* p_filename)
{
	const unsigned int BUFFER_SIZE = 512;

	char buffer[BUFFER_SIZE];
	memset(buffer, 0, sizeof(char) * BUFFER_SIZE);

	GLsizei length = 0;

	glGetShaderInfoLog(p_shader, BUFFER_SIZE, &length, buffer);

	if (length > 0)
	{
		Utils::Log("Shader %s:\n%s", p_filename, buffer);
	}
}

void Shader::validateProgram(GLuint p_program)
{
	const unsigned int BUFFER_SIZE = 512;

	char buffer[BUFFER_SIZE];
	memset(buffer, 0, sizeof(char)* BUFFER_SIZE);

	GLsizei length = 0;

	glGetProgramInfoLog(p_program, BUFFER_SIZE, &length, buffer);

	if (length > 0)
	{
		Utils::Log("Program %i:\n%s", shader_id, buffer);
	}

}