#pragma once
#include <windows.h>
#include <stdio.h>

#include <gl/glew.h>
#include <gl/gl.h>
#include <gl/wglew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "FreeImage.lib")
