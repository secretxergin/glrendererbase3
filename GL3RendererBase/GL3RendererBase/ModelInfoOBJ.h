#pragma once

#include <stdio.h>
#include <vector>

struct VertexPositionInfo
{
	float x;
	float y;
	float z;
};

struct VertexNormalInfo
{
	float nx;
	float ny;
	float nz;
};

struct VertexUVInfo
{
	float u;
	float v;
};

struct IndexInfo
{
	int pos;
	int uv;
	int normal;
};

class ModelInfoOBJ
{
private:
	std::vector<VertexPositionInfo>				m_positions;
	std::vector<VertexUVInfo>					m_uvs;
	std::vector<VertexNormalInfo>				m_normals;
	std::vector<unsigned int>					m_indices;

	void										init(const char* p_filename);

public:	
	ModelInfoOBJ();
	ModelInfoOBJ(const char* p_filename);
	~ModelInfoOBJ();

	unsigned int								getPositionFloatCount();
	unsigned int								getUVFloatCount();
	unsigned int								getNormalFloatCount();
	unsigned int								getIndexCount();
	

	void										copyPositionDataToBuffer(float * p_buffer);
	void										copyUVDataToBuffer(float * p_buffer);
	void										copyNormalDataToBuffer(float * p_buffer);
	void										copyIndexDataToBuffer(int * p_buffer);

};