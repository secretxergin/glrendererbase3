#pragma once

#include <stdlib.h>
#include <GL/glew.h>
#include <GL/gl.h>

#include <string>

class Shader
{
public:
	Shader(void);
	Shader(const char* p_vsFile, const char* p_fsFile);
	~Shader(void);

	void					init(const char* p_vsFile, const char* p_fsFile);

	void					bind(void);
	void					unbind(void);

	unsigned int			id(void);

private:
	unsigned int			shader_id;
	unsigned int			shader_vp;
	unsigned int			shader_fp;

	void					validateShader(GLuint p_shader, const char* p_filename);
	void					validateProgram(GLuint p_program);
};