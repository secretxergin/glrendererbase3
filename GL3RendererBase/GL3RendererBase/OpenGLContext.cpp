#include "OpenGLContext.h"
#include "Utilities.h"
#include "ModelInfoOBJ.h"
#include "FreeImage.h"

OpenGLContext::OpenGLContext(void)
{

}

OpenGLContext::OpenGLContext(HWND p_hwnd)
{
	createOpenGLContextv30(p_hwnd);
}

OpenGLContext::~OpenGLContext(void)
{
	wglMakeCurrent(m_hdc, 0);
	wglDeleteContext(m_hrc);

	ReleaseDC(m_hwnd, m_hdc);
}

bool OpenGLContext::createOpenGLContextv30(HWND p_hwnd)
{
	m_hwnd = p_hwnd;
	m_hdc = GetDC(m_hwnd);

	// ----------------------------------------
	// |        Pixel Format Descriptor       |
	// ----------------------------------------
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize		=	sizeof(PIXELFORMATDESCRIPTOR);
	pfd.dwFlags		=	PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType	=	PFD_TYPE_RGBA;
	pfd.cColorBits	=	32;
	pfd.cDepthBits	=	32;
	pfd.iLayerType	=	PFD_MAIN_PLANE;


	int nPixelFormat = ChoosePixelFormat(m_hdc, &pfd);
	if (nPixelFormat == 0)
	{
		OutputDebugString(L"Invalid Pixel Format!\n");
		return false;
	}

	BOOL b_result = SetPixelFormat(m_hdc, nPixelFormat, &pfd);
	if (!b_result)
	{
		return false;
	}

	HGLRC temporaryGLContext = wglCreateContext(m_hdc);
	wglMakeCurrent(m_hdc, temporaryGLContext);

	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		return false;
	}

	int attributes[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0
	};

	if (wglewIsSupported("WGL_ARB_create_context") == 1)
	{
		m_hrc = wglCreateContextAttribsARB(m_hdc, NULL, attributes);
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(temporaryGLContext);
		wglMakeCurrent(m_hdc, m_hrc);
	}
	else
	{
		return false;
	}

	int glVersion[2] = { -1, -1 };
	glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]); 
	glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]); 

	Utils::Log("GL Version %i.%i\n", glVersion[0], glVersion[1]);

	return true;
}


void OpenGLContext::reshapeWindow(int p_width, int p_height)
{
	m_windowWidth = p_width;
	m_windowHeight = p_height;
}

void OpenGLContext::setupScene(void)
{
	glClearColor(0.439f, 0.572f, 0.745f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_TEXTURE_2D);


	m_shader = new Shader("Resources/test.vsh", "Resources/test.fsh");
	
	// + Setup initial matrices
	m_projectionMatrix = glm::perspective(45.0f, (float)m_windowWidth / (float)m_windowHeight, 0.1f, 100.0f);
	m_viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -2.0f, -12.0f));
	m_modelMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(5.0f, 5.0f, 5.0f));
	yrot = 0.0f;
	// - Matrices
	
	
	
	m_shader->bind();

	loadOBJ("Resources/trashcan-maya.obj");

	// +Temporary Texture Loading Code
	FreeImage_Initialise(true);

	GLint normalMapLocation = glGetUniformLocation(m_shader->id(), "normalMap");

	FIBITMAP * imgData = FreeImage_Load(FIF_TARGA, "Resources/brg-ext-trashcan_normal.tga");
	unsigned int width = FreeImage_GetWidth(imgData);
	unsigned int height = FreeImage_GetHeight(imgData);
	unsigned int bitsPerPixel = FreeImage_GetBPP(imgData);
	FREE_IMAGE_COLOR_TYPE colorType = FreeImage_GetColorType(imgData);
	//Utils::Log("width: %i \theight %i \tdepth %i\n", width, height, bitsPerPixel);
	GLbyte * pixelData = (GLbyte*) FreeImage_GetBits(imgData);// (GLbyte*)malloc(sizeof(GLbyte)*width*height * 3);
	
	glGenTextures(4, m_textures);
	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, m_textures[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, pixelData);
	glUniform1i(normalMapLocation, 0);
	FreeImage_Unload(imgData);


	GLint colorMapLocation = glGetUniformLocation(m_shader->id(), "colorMap");
	imgData = FreeImage_Load(FIF_TARGA, "Resources/brg-ext-trashcan_color.tga");
	width = FreeImage_GetWidth(imgData);
	height = FreeImage_GetHeight(imgData);
	bitsPerPixel = FreeImage_GetBPP(imgData);
	colorType = FreeImage_GetColorType(imgData);
	//Utils::Log("width: %i \theight %i \tdepth %i\n", width, height, bitsPerPixel);
	pixelData = (GLbyte*)FreeImage_GetBits(imgData);// (GLbyte*)malloc(sizeof(GLbyte)*width*height * 3);

	glGenTextures(4, m_textures);
	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(GL_TEXTURE_2D, m_textures[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, pixelData);
	glUniform1i(colorMapLocation, 1);
	FreeImage_Unload(imgData);
	
	//FreeImage_Unload(imgData);
	FreeImage_DeInitialise();
	//- Texture Loading Code

	m_shader->unbind();
}

void OpenGLContext::renderScene(void)
{
	glViewport(0, 0, m_windowWidth, m_windowHeight);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	m_shader->bind();

	// + Spin!
	yrot += 0.0005f;
	m_modelMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(5.0f, 5.0f, 5.0f));
	m_modelMatrix = glm::rotate(m_modelMatrix, yrot, glm::vec3(0.0f, 1.0f, 0.0f));
	// - Spin

	// + Get shader variable addresses
	int projectionMatrixLocation = glGetUniformLocation(m_shader->id(), "projectionMatrix");
	int viewMatrixLocation = glGetUniformLocation(m_shader->id(), "viewMatrix");
	int modelMatrixLocation = glGetUniformLocation(m_shader->id(), "modelMatrix");
	// -

	// + Pass matrices to shader 
	glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, &m_projectionMatrix[0][0]);
	glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &m_viewMatrix[0][0]);
	glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &m_modelMatrix[0][0]);
	// -

	glBindVertexArray(m_vertexArrayObject[0]);
	glDrawElements(GL_TRIANGLES, 2400, GL_UNSIGNED_INT, (void*)0);
	glBindVertexArray(0);

	m_shader->unbind();

	SwapBuffers(m_hdc);
}

void OpenGLContext::createQuad(void)
{
	float * positions =  new float[18];
	float * colors = new float[18];
	float * uvs = new float[12];

	positions[0] = -0.5f;	positions[1] = -0.5f;	positions[2] = 0.0f;
	colors[0] = 0.0f;		colors[1] = 1.0f;		colors[2] = 0.0f;
	uvs[0] = 1.0f;			uvs[1] = 0.0f;
	positions[3] = -0.5f;	positions[4] = 0.5f;	positions[5] = 0.0f;
	colors[3] = 0.0f;		colors[4] = 1.0f;		colors[5] = 0.0f;
	uvs[2] = 0.0f;			uvs[3] = 1.0f;
	positions[6] = 0.5f;	positions[7] = 0.5f;	positions[8] = 0.0f;
	colors[6] = 0.0f;		colors[7] = 1.0f;		colors[8] = 0.0f;
	uvs[4] = 0.0f;			uvs[5] = 0.0f;

	positions[9] = 0.5f;	positions[10] = -0.5f;	positions[11] = 0.0f;
	colors[9] = 0.0f;		colors[10] = 1.0f;		colors[11] = 0.0f;
	uvs[6] = 0.0f;			uvs[7] = 0.0f;
	positions[12] = -0.5f;	positions[13] = -0.5f;	positions[14] = 0.0f;
	colors[12] = 0.0f;		colors[13] = 1.0f;		colors[14] = 0.0f;
	uvs[8] = 0.0f;			uvs[9] = 0.0f;
	positions[15] = 0.5f;	positions[16] = 0.5f;	positions[17] = 0.0f;
	colors[15] = 0.0f;		colors[16] = 1.0f;		colors[17] = 0.0f;
	uvs[10] = 0.0f;			uvs[11] = 0.0f;

	glGenVertexArrays(1, m_vertexArrayObject);
	glBindVertexArray(m_vertexArrayObject[0]);

	glGenBuffers(3, m_vertexBufferObject);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[0]);
	glBufferData(GL_ARRAY_BUFFER, 18*sizeof(GLfloat), positions, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[1]);
	glBufferData(GL_ARRAY_BUFFER, 18 * sizeof(GLfloat), colors, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[2]);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat), uvs, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(2);

	glBindVertexArray(0);

	delete[] positions;
	delete[] colors;
	delete[] uvs;
}

void OpenGLContext::loadOBJ(const char * p_filename)
{
	ModelInfoOBJ * model = new ModelInfoOBJ(p_filename);

	float * positions = (float*)malloc(sizeof(float) * model->getPositionFloatCount());
	float * uvs = (float*)malloc(sizeof(float) * model->getUVFloatCount());;
	float * normals = (float*)malloc(sizeof(float) * model->getNormalFloatCount());;
	int * indices = (int*)malloc(sizeof(int) * model->getIndexCount());

	model->copyPositionDataToBuffer(positions);
	model->copyUVDataToBuffer(uvs);
	model->copyNormalDataToBuffer(normals);
	model->copyIndexDataToBuffer(indices);
	
	glGenVertexArrays(1, m_vertexArrayObject);
	glBindVertexArray(m_vertexArrayObject[0]);

	glGenBuffers(4, m_vertexBufferObject);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[0]);
	glBufferData(GL_ARRAY_BUFFER, model->getPositionFloatCount() * sizeof(GLfloat), positions, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[1]);
	glBufferData(GL_ARRAY_BUFFER, model->getNormalFloatCount() * sizeof(GLfloat), normals, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[2]);
	glBufferData(GL_ARRAY_BUFFER, model->getUVFloatCount() * sizeof(GLfloat), uvs, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vertexBufferObject[3]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, model->getIndexCount() * sizeof(GLint), indices, GL_STATIC_DRAW);

	glBindVertexArray(0);

	free(positions);
	free(uvs);
	free(normals);
	free(indices);
}