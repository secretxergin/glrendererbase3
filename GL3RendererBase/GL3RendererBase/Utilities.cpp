#include "Utilities.h"

namespace Utils
{
	void Log(const char* p_formattedString, ...)
	{
		char buffer[256];
		va_list args;
		va_start(args, p_formattedString);
		_vsnprintf_s(buffer, sizeof(buffer), p_formattedString, args);
		va_end(args);

		OutputDebugStringA(buffer);
	}


	// WARNING: Not safe. Free the allocated char* after use!
	char* ReadTextFile(const char* p_filename)
	{
		// Only reads ANSI encoded input files

		char* resultingText=NULL;

		if (p_filename != NULL)
		{
			FILE * fileToRead;
			fopen_s(&fileToRead, p_filename, "rt");

			if (fileToRead != NULL)
			{
				fseek(fileToRead, 0, SEEK_END);
				int count = ftell(fileToRead);
				rewind(fileToRead);

				if (count > 0)
				{
					resultingText = (char*)malloc(sizeof(char)* (count + 1));
					count = fread(resultingText, sizeof(char), count, fileToRead);
					resultingText[count] = '\0';
				}

				fclose(fileToRead);
			}
		}
		return resultingText;
	}


}