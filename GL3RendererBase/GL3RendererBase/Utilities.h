#include <Windows.h>
#include <stdio.h>
#include <stdarg.h>

namespace Utils
{
	void				Log(const char* p_formattedString, ...);
	char*				ReadTextFile(const char* p_filename);
}