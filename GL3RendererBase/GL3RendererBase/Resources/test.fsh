#version 150 core
in vec3 pass_Color;
in vec2 pass_UVs;
out vec4 out_Color;

uniform sampler2D normalMap;
uniform sampler2D colorMap;

void main(void)
{
	//out_Color = vec4 ( pass_Color, 1.0);
	out_Color = texture( colorMap, pass_UVs );
}